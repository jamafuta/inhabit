from braces.views import LoginRequiredMixin, PermissionRequiredMixin
from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView
from inhabit.apps.room_manager.models import Occupant, Schedule, OccupantType, RoomType, \
    Room, Block, Expense, ExpenseCategory, RentPayment


class ActiveNavBarMixin(LoginRequiredMixin, PermissionRequiredMixin):
    def get_context_data(self, **kwargs):
        context = super(ActiveNavBarMixin, self).get_context_data(**kwargs)
        context[self.nav_group_name] = "active"
        context[self.nav_item_name] = "active"
        return context


class OccupantMixin(ActiveNavBarMixin):
    nav_group_name = "room_management_active"
    nav_item_name = "occupants_active"
    model = Occupant


class ScheduleMixin(ActiveNavBarMixin):
    nav_group_name = "room_management_active"
    nav_item_name = "schedules_active"
    model = Schedule


class OccupantTypeMixin(ActiveNavBarMixin):
    nav_group_name = "room_management_active"
    nav_item_name = "occupant_types_active"
    model = OccupantType


class RoomTypeMixin(ActiveNavBarMixin):
    nav_group_name = "room_management_active"
    nav_item_name = "room_types_active"
    model = RoomType


class RoomMixin(ActiveNavBarMixin):
    nav_group_name = "room_management_active"
    nav_item_name = "rooms_active"
    model = Room


class BlockMixin(ActiveNavBarMixin):
    nav_group_name = "room_management_active"
    nav_item_name = "blocks_active"
    model = Block


class ExpenseMixin(ActiveNavBarMixin):
    nav_group_name = "expenses_group_active"
    nav_item_name = "expenses_active"
    model = Expense


class ExpenseCategoryMixin(ActiveNavBarMixin):
    nav_group_name = "expenses_group_active"
    nav_item_name = "expense_category_active"
    model = ExpenseCategory


class RentPaymentMixin(ActiveNavBarMixin):
    nav_group_name = "rent_group_active"
    nav_item_name = "rent_payment_active"
    model = RentPayment


class InhabitListView(ListView):
    paginate_by = 20
    def __init__(self, **kwargs):
        super(InhabitListView, self).__init__(**kwargs)
        self.permission_required = "room_manager.view_%s" % self.model.__name__.lower()


class InhabitCreateView(CreateView):
    def __init__(self, **kwargs):
        super(InhabitCreateView, self).__init__(**kwargs)
        self.permission_required = "room_manager.add_%s" % self.model.__name__.lower()


class InhabitDetailView(DetailView):
    def __init__(self, **kwargs):
        super(InhabitDetailView, self).__init__(**kwargs)
        self.permission_required = "room_manager.view_%s" % self.model.__name__.lower()


class InhabitUpdateView(UpdateView):
    def __init__(self, **kwargs):
        super(InhabitUpdateView, self).__init__(**kwargs)
        self.permission_required = "room_manager.change_%s" % self.model.__name__.lower()


class InhabitDeleteView(DeleteView):
    def __init__(self, **kwargs):
        super(InhabitDeleteView, self).__init__(**kwargs)
        self.permission_required = "room_manager.delete_%s" % self.model.__name__.lower()
from decimal import Decimal
from model_report.utils import sum_column, date_format
from inhabit.apps.room_manager.models import Expense, RentPayment, Room
from model_report.report import reports, ReportAdmin


def ugx_format(value, instance):
    """
    Format cell value to money
    """
    return 'UGX %.0f' % Decimal(value)


class ExpenseReport(ReportAdmin):
    title = ('Expense Report')
    model = Expense
    fields = [
        'submitted', 'amount', 'category', 'description', 'user__username', 'submitted_by',
    ]
    list_order_by = ('submitted', 'amount')
    list_filter = ('user__username', 'category', 'submitted')
    list_group_by = ('category', 'submitted_by', 'user__username', 'submitted')
    type = 'report'
    group_totals = {'amount': sum_column}
    override_field_formats = {'submitted': date_format, 'amount': ugx_format}
    override_field_labels = {
        'user__username': lambda x, y: ('User'),
    }


class RentPaymentReport(ReportAdmin):
    title = ('Rent Payment Report')
    model = RentPayment
    fields = [
        'occupant__full_name', 'amount_paid', 'room', 'room__room_type__name', 'room__block__name', 'user__username',
        'submitted',
        'method_of_payment'
    ]
    list_order_by = ('room', 'amount_paid')
    list_filter = ('user__username', 'occupant__full_name', 'room', 'submitted')
    list_group_by = (
        'room', 'occupant__full_name', 'user__username', 'submitted', 'room__room_type__name', 'room__block__name')
    type = 'report'
    group_totals = {'amount_paid': sum_column}
    override_field_formats = {'submitted': date_format, 'amount_paid': ugx_format}
    override_field_labels = {
        'room__room_type__name': lambda x, y: ('Room Type'),
        'room__block__name': lambda x, y: ('Block'),
    }


class RoomAvailabilityReport(ReportAdmin):
    title = ('Room Availability Report')
    model = Room
    fields = [
        'room_number','block__name','room_type__name'
    ]
    override_field_labels = {
        'room_type__name': lambda x, y: ('Room Type'),
        'block__name': lambda x, y: ('Block'),
    }
    def filter_query(self, qs):
        return super(RoomAvailabilityReport, self).filter_query(qs)


reports.register('expenses', ExpenseReport)
reports.register('rent_payments', RentPaymentReport)
reports.register('rooms', RoomAvailabilityReport)


from django.contrib.auth.models import User, Permission
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models.signals import post_syncdb
from model_utils import Choices

from model_utils.models import TimeStampedModel, StatusModel
import select2.fields


class Block(TimeStampedModel):
    name = models.CharField(max_length=255)

    def get_absolute_url(self):
        return reverse("room_manager.blocks.show", kwargs={"pk": self.id})

    def __unicode__(self):
        return self.name


class RoomType(TimeStampedModel):
    name = models.CharField(max_length=255)
    number_of_occupants = models.IntegerField()
    rate = models.IntegerField()

    def get_absolute_url(self):
        return reverse("room_manager.room_types.show", kwargs={"pk": self.id})

    def __unicode__(self):
        return self.name


class Room(TimeStampedModel):
    room_type = select2.fields.ForeignKey(RoomType, related_name="rooms", overlay="Choose a room type...", ajax=True,
                                          search_field='name', )
    room_number = models.CharField(max_length=50, unique=True)
    block = select2.fields.ForeignKey(Block, overlay="Choose a block...", ajax=True, search_field='name', )

    @property
    def available(self):
        return self.occupants.all().count() < 1

    def get_absolute_url(self):
        return reverse("room_manager.rooms.show", kwargs={"pk": self.id})

    def __unicode__(self):
        return self.room_number


class Schedule(TimeStampedModel):
    unit_of_time = models.CharField(max_length=80)
    frequency = models.IntegerField()
    start_date = models.DateField()

    def get_absolute_url(self):
        return reverse("room_manager.schedules.show", kwargs={"pk": self.id})

    def __unicode__(self):
        return "every %s %s  starting %s" % (self.frequency, self.unit_of_time, self.start_date)


class OccupantType(TimeStampedModel):
    name = models.CharField(max_length=80)
    schedule = select2.fields.ForeignKey(Schedule, related_name="occupant_types")

    def get_absolute_url(self):
        return reverse("room_manager.occupant_types.show", kwargs={"pk": self.id})

    def __unicode__(self):
        return self.name


class Occupant(TimeStampedModel):
    full_name = models.CharField(max_length=255)
    room = select2.fields.ForeignKey(Room, related_name="occupants")
    occupant_type = select2.fields.ForeignKey(OccupantType, related_name="occupants")
    occupation = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=80, unique=True)
    id_number = models.CharField(max_length=80)
    id_issuing_authority = models.CharField(max_length=80)

    def get_absolute_url(self):
        return reverse("room_manager.occupants.show", kwargs={"pk": self.id})

    def __unicode__(self):
        return self.full_name

        #class Meta:
        #    unique_together = (("room", "phone_number"))


class ExpenseCategory(TimeStampedModel):
    name = models.CharField(max_length=80)

    def get_absolute_url(self):
        return reverse("room_manager.expense_categories.show", kwargs={"pk": self.id})

    def __unicode__(self):
        return self.name


class Expense(TimeStampedModel):
    submitted = models.DateTimeField()
    user = select2.fields.ForeignKey(User, related_name="expenses", overlay="Choose a user...",
                                     ajax=True,
                                     search_field='username', )
    submitted_by = models.CharField(max_length=80)
    description = models.CharField(max_length=255)
    amount = models.IntegerField()
    category = select2.fields.ForeignKey(ExpenseCategory, related_name="expenses", overlay="Choose a category...",
                                         ajax=True,
                                         search_field='name', )

    def get_absolute_url(self):
        return reverse("room_manager.expenses.show", kwargs={"pk": self.id})

    def __unicode__(self):
        return "%s by %s on %s " % (self.description, self.submitted_by, self.submitted)


class RentPayment(TimeStampedModel, StatusModel):
    occupant = select2.fields.ForeignKey(Occupant, ajax=True, search_field='full_name',
                                         overlay="Choose an occupant...", )
    date_due = models.DateField(null=True)
    amount_due = models.IntegerField(default=0)
    amount_paid = models.IntegerField(default=0)
    room = select2.fields.ForeignKey(Room, related_name="rent_payments", ajax=True, search_field='room_number',
                                     overlay="Choose a room...", )
    user = select2.fields.ForeignKey(User, related_name="rent_payments", ajax=True, search_field='username',
                                     overlay="Choose a user...", )
    method_of_payment = models.CharField(null=True, max_length=255)
    payment_detail = models.CharField(max_length=255)
    submitted = models.DateTimeField()
    STATUS = Choices('not_paid', 'paid', )

    def get_absolute_url(self):
        return reverse("room_manager.rent_payments.show", kwargs={"pk": self.id})


def add_view_permissions(sender, **kwargs):
    for content_type in ContentType.objects.all():
        codename = "view_%s" % content_type.model
        if not Permission.objects.filter(content_type=content_type, codename=codename):
            Permission.objects.create(content_type=content_type,
                                      codename=codename,
                                      name="Can view %s" % content_type.name)
            print "Added view permission for %s" % content_type.name


post_syncdb.connect(add_view_permissions)

from braces.views import LoginRequiredMixin
from django.db.models import Count
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView
from inhabit.apps.room_manager.forms import ExpenseForm, ScheduleForm, RentPaymentForm
from inhabit.apps.room_manager.mixins import OccupantMixin, ScheduleMixin, OccupantTypeMixin, RoomTypeMixin, \
    RoomMixin, BlockMixin, ExpenseMixin, ExpenseCategoryMixin, RentPaymentMixin, InhabitListView as ListView, \
    InhabitCreateView as CreateView, InhabitDetailView as DetailView, InhabitUpdateView as UpdateView, \
    InhabitDeleteView as DeleteView
from inhabit.apps.room_manager.models import Room


class HomeView(LoginRequiredMixin, TemplateView):
    template_name = "room_manager/home.html"


class ListRoomsView(RoomMixin, ListView):
    paginate_by = 20
    context_object_name = 'rooms'


class RoomDetailView(RoomMixin, DetailView):
    pass


class CreateRoomView(RoomMixin, CreateView):
    pass


class RoomUpdateView(RoomMixin, UpdateView):
    pass


class RoomDeleteView(RoomMixin, DeleteView):
    success_url = "/rooms"


class ListBlocksView(BlockMixin, ListView):
    paginate_by = 20
    context_object_name = 'blocks'


class CreateBlockView(BlockMixin, CreateView):
    pass


class BlockDetailView(BlockMixin, DetailView):
    pass


class BlockUpdateView(BlockMixin, UpdateView):
    pass


class BlockDeleteView(BlockMixin, DeleteView):
    success_url = "/blocks"


class RoomTypeListView(RoomTypeMixin, ListView):
    paginate_by = 20
    context_object_name = 'room_types'


class RoomTypeCreateView(RoomTypeMixin, CreateView):
    pass


class RoomTypeDetailView(RoomTypeMixin, DetailView):
    pass


class RoomTypeUpdateView(RoomTypeMixin, UpdateView):
    pass


class RoomTypeDeleteView(RoomTypeMixin, DeleteView):
    success_url = "/roomtypes"


OccupantTypeListView = type("OccupantTypeListView", (OccupantTypeMixin, ListView),
                            {"paginate_by": 20, "context_object_name": 'occupant_types'})

OccupantTypeCreateView = type("OccupantTypeCreateView", (OccupantTypeMixin, CreateView), {})
OccupantTypeDetailView = type("OccupantTypeDetailView", (OccupantTypeMixin, DetailView), {})
OccupantTypeUpdateView = type("OccupantTypeUpdateView", (OccupantTypeMixin, DetailView), {})
OccupantTypeDeleteView = type("OccupantTypeDeleteView", (OccupantTypeMixin, DeleteView),
                              {"success_url": "/occupanttypes"})


class ListSchedulesView(ScheduleMixin, ListView):
    paginate_by = 20
    context_object_name = 'schedules'


class CreateScheduleView(ScheduleMixin, CreateView):
    form_class = ScheduleForm


class ScheduleDetailView(ScheduleMixin, DetailView):
    pass


class ScheduleUpdateView(ScheduleMixin, UpdateView):
    form_class = ScheduleForm


class ScheduleDeleteView(ScheduleMixin, DeleteView):
    success_url = "/schedules"


class OccupantListView(OccupantMixin, ListView):
    paginate_by = 20
    context_object_name = 'occupants'


class OccupantCreateView(OccupantMixin, CreateView):
    pass


class OccupantDetailView(OccupantMixin, DetailView):
    pass


class OccupantUpdateView(OccupantMixin, UpdateView):
    pass


class OccupantDeleteView(OccupantMixin, DeleteView):
    success_url = "/occupants"


class ExpenseListView(ExpenseMixin, ListView):
    paginate_by = 20
    context_object_name = 'expenses'


class ExpenseCreateView(ExpenseMixin, CreateView):
    form_class = ExpenseForm

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.save()
        return HttpResponseRedirect(obj.get_absolute_url())


class ExpenseDetailView(ExpenseMixin, DetailView):
    pass


class ExpenseUpdateView(ExpenseMixin, UpdateView):
    form_class = ExpenseForm


class ExpenseDeleteView(ExpenseMixin, DeleteView):
    success_url = "/expenses"


class ExpenseCategoryListView(ExpenseCategoryMixin, ListView):
    paginate_by = 20
    context_object_name = 'expense_categories'


class ExpenseCategoryCreateView(ExpenseCategoryMixin, CreateView):
    pass


class ExpenseCategoryDetailView(ExpenseCategoryMixin, DetailView):
    pass


class ExpenseCategoryUpdateView(ExpenseCategoryMixin, UpdateView):
    form_class = RentPaymentForm


class ExpenseCategoryDeleteView(ExpenseCategoryMixin, DeleteView):
    success_url = "/expense_categories"


class RentPaymentListView(RentPaymentMixin, ListView):
    paginate_by = 20
    context_object_name = 'rent_payments'


class RentPaymentCreateView(RentPaymentMixin, CreateView):
    form_class = RentPaymentForm

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.save()
        return HttpResponseRedirect(obj.get_absolute_url())


class RentPaymentDetailView(RentPaymentMixin, DetailView):
    pass


class RentPaymentUpdateView(RentPaymentMixin, UpdateView):
    pass


class RentPaymentDeleteView(RentPaymentMixin, DeleteView):
    success_url = "/rent_payments"


#REPORT VIEWS
class ExpensesReportView(ExpenseMixin, ListView):
    template_name = "room_manager/expense_report.html"

    def get_template_names(self):
        return self.template_name


class RoomReportView(ListRoomsView):
    template_name = "room_manager/room_availability.html"

    def get_queryset(self):
        status = self.request.GET['status']
        print status
        if status == "1":
            print "avv"
            return Room.objects.annotate(num_occupants=Count('occupants')).filter(num_occupants__lt=1)
        elif status == "0":
            return Room.objects.annotate(num_occupants=Count('occupants')).filter(num_occupants__gt=0)
        return Room.objects.all()




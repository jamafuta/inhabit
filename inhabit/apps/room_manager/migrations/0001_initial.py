# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Block'
        db.create_table(u'room_manager_block', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'room_manager', ['Block'])

        # Adding model 'RoomType'
        db.create_table(u'room_manager_roomtype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('number_of_occupants', self.gf('django.db.models.fields.IntegerField')()),
            ('rate', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'room_manager', ['RoomType'])

        # Adding model 'Room'
        db.create_table(u'room_manager_room', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('room_type', self.gf('django.db.models.fields.related.ForeignKey')(related_name='rooms', to=orm['room_manager.RoomType'])),
            ('room_number', self.gf('django.db.models.fields.CharField')(unique=True, max_length=50)),
            ('block', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['room_manager.Block'])),
        ))
        db.send_create_signal(u'room_manager', ['Room'])

        # Adding model 'Schedule'
        db.create_table(u'room_manager_schedule', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('unit_of_time', self.gf('django.db.models.fields.CharField')(max_length=80)),
            ('frequency', self.gf('django.db.models.fields.IntegerField')()),
            ('start_date', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal(u'room_manager', ['Schedule'])

        # Adding model 'OccupantType'
        db.create_table(u'room_manager_occupanttype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=80)),
            ('schedule', self.gf('django.db.models.fields.related.ForeignKey')(related_name='occupant_types', to=orm['room_manager.Schedule'])),
        ))
        db.send_create_signal(u'room_manager', ['OccupantType'])

        # Adding model 'Occupant'
        db.create_table(u'room_manager_occupant', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('full_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('occupation', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('phone_number', self.gf('django.db.models.fields.CharField')(max_length=80)),
            ('id_number', self.gf('django.db.models.fields.CharField')(max_length=80)),
            ('id_issuing_authority', self.gf('django.db.models.fields.CharField')(max_length=80)),
            ('room', self.gf('django.db.models.fields.related.ForeignKey')(related_name='occupants', to=orm['room_manager.Room'])),
        ))
        db.send_create_signal(u'room_manager', ['Occupant'])

        # Adding model 'ExpenseCategory'
        db.create_table(u'room_manager_expensecategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=80)),
        ))
        db.send_create_signal(u'room_manager', ['ExpenseCategory'])

        # Adding model 'Expense'
        db.create_table(u'room_manager_expense', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('submitted', self.gf('django.db.models.fields.DateTimeField')()),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('submitted_by', self.gf('django.db.models.fields.CharField')(max_length=80)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('amount', self.gf('django.db.models.fields.IntegerField')()),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(related_name='expenses', to=orm['room_manager.ExpenseCategory'])),
        ))
        db.send_create_signal(u'room_manager', ['Expense'])

        # Adding model 'RentPayment'
        db.create_table(u'room_manager_rentpayment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('status', self.gf('model_utils.fields.StatusField')(default='not_paid', max_length=100, no_check_for_status=True)),
            ('status_changed', self.gf('model_utils.fields.MonitorField')(default=datetime.datetime.now, monitor=u'status')),
            ('occupant', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['room_manager.Occupant'])),
            ('date_due', self.gf('django.db.models.fields.DateField')()),
            ('amount_due', self.gf('django.db.models.fields.IntegerField')()),
            ('amount_paid', self.gf('django.db.models.fields.IntegerField')()),
            ('room', self.gf('django.db.models.fields.related.ForeignKey')(related_name='rent_payments', to=orm['room_manager.Room'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('method_of_payment', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('payment_detail', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'room_manager', ['RentPayment'])


    def backwards(self, orm):
        # Deleting model 'Block'
        db.delete_table(u'room_manager_block')

        # Deleting model 'RoomType'
        db.delete_table(u'room_manager_roomtype')

        # Deleting model 'Room'
        db.delete_table(u'room_manager_room')

        # Deleting model 'Schedule'
        db.delete_table(u'room_manager_schedule')

        # Deleting model 'OccupantType'
        db.delete_table(u'room_manager_occupanttype')

        # Deleting model 'Occupant'
        db.delete_table(u'room_manager_occupant')

        # Deleting model 'ExpenseCategory'
        db.delete_table(u'room_manager_expensecategory')

        # Deleting model 'Expense'
        db.delete_table(u'room_manager_expense')

        # Deleting model 'RentPayment'
        db.delete_table(u'room_manager_rentpayment')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'room_manager.block': {
            'Meta': {'object_name': 'Block'},
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'room_manager.expense': {
            'Meta': {'object_name': 'Expense'},
            'amount': ('django.db.models.fields.IntegerField', [], {}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'expenses'", 'to': u"orm['room_manager.ExpenseCategory']"}),
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'submitted': ('django.db.models.fields.DateTimeField', [], {}),
            'submitted_by': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'room_manager.expensecategory': {
            'Meta': {'object_name': 'ExpenseCategory'},
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80'})
        },
        u'room_manager.occupant': {
            'Meta': {'object_name': 'Occupant'},
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_issuing_authority': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'id_number': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'occupation': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'room': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'occupants'", 'to': u"orm['room_manager.Room']"})
        },
        u'room_manager.occupanttype': {
            'Meta': {'object_name': 'OccupantType'},
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'schedule': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'occupant_types'", 'to': u"orm['room_manager.Schedule']"})
        },
        u'room_manager.rentpayment': {
            'Meta': {'object_name': 'RentPayment'},
            'amount_due': ('django.db.models.fields.IntegerField', [], {}),
            'amount_paid': ('django.db.models.fields.IntegerField', [], {}),
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'date_due': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'method_of_payment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'occupant': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['room_manager.Occupant']"}),
            'payment_detail': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'room': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rent_payments'", 'to': u"orm['room_manager.Room']"}),
            'status': ('model_utils.fields.StatusField', [], {'default': "'not_paid'", 'max_length': '100', u'no_check_for_status': 'True'}),
            'status_changed': ('model_utils.fields.MonitorField', [], {'default': 'datetime.datetime.now', u'monitor': "u'status'"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'room_manager.room': {
            'Meta': {'object_name': 'Room'},
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['room_manager.Block']"}),
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'room_number': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'room_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rooms'", 'to': u"orm['room_manager.RoomType']"})
        },
        u'room_manager.roomtype': {
            'Meta': {'object_name': 'RoomType'},
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'number_of_occupants': ('django.db.models.fields.IntegerField', [], {}),
            'rate': ('django.db.models.fields.IntegerField', [], {})
        },
        u'room_manager.schedule': {
            'Meta': {'object_name': 'Schedule'},
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'frequency': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'start_date': ('django.db.models.fields.DateField', [], {}),
            'unit_of_time': ('django.db.models.fields.CharField', [], {'max_length': '80'})
        }
    }

    complete_apps = ['room_manager']
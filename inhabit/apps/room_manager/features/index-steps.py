from lettuce import *


@before.all
def set_browser():
    pass


@step(r'I access the url "(.*)"')
def access_url(step, url):
    response = world.browser.get(url)


@step(r'I see the header "(.*)"')
def see_header(step, text):
    header = world.dom.cssselect('h1')[0]
from django.forms import ModelForm, CharField, TextInput
from inhabit.apps.room_manager.models import Expense, Schedule, RentPayment


class ScheduleForm(ModelForm):
    start_date = CharField(widget=TextInput(attrs={'class': 'datepicker'}))

    class Meta:
        def __init__(self):
            pass

        model = Schedule


class ExpenseForm(ModelForm):
    submitted = CharField(widget=TextInput(attrs={'class': 'datepicker'}))

    class Meta:
        def __init__(self):
            pass

        exclude = ('user',)
        model = Expense


class RentPaymentForm(ModelForm):
    submitted = CharField(widget=TextInput(attrs={'class': 'datepicker'}))

    class Meta:
        def __init__(self):
            pass
        exclude = ('user', 'status', 'status_changed', 'amount_due', 'date_due')
        model = RentPayment




